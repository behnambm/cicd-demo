package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"net/url"
	"os"
	"strings"
)

// new in merge request 2

type AuthData struct {
	Username string `form:"username" binding:"required"`
	Password string `form:"password" binding:"required"`
}

func index(c *gin.Context) {
	c.HTML(http.StatusOK, "index.gohtml", gin.H{"PageTitle": "hello"})

}

func ping(c *gin.Context) {
	c.JSON(http.StatusOK, map[string]string{"status": "ok"})
}

func admin(c *gin.Context) {
	is_verified, err := c.Cookie("is_verified")
	if err != nil || is_verified != "true" {
		c.HTML(http.StatusUnauthorized, "index.gohtml", gin.H{"error": "not authorized"})
		return
	}
	c.HTML(http.StatusOK, "admin.gohtml", gin.H{"message": "hello admin"})
}

func auth(c *gin.Context) {
	var authData AuthData

	if err := c.ShouldBind(&authData); err != nil {
		c.HTML(http.StatusBadRequest, "index.gohtml", gin.H{"error": "invalid request"})
		return
	}

	if strings.ToLower(authData.Username) != "admin" || strings.ToLower(authData.Password) != "admin" {
		c.HTML(http.StatusUnauthorized, "index.gohtml", gin.H{"error": "invalid username or password"})
		return
	}

	c.SetCookie("is_verified", "true", 3600, "/admin", "", false, true)
	location := url.URL{Path: "/admin"}
	c.Redirect(http.StatusFound, location.RequestURI())
}

func main() {
	port := os.Getenv("PORT")
	router := gin.Default()
	router.LoadHTMLGlob("templates/*")

	router.GET("/ping", ping)
	router.GET("/admin", admin)
	router.POST("/auth", auth)
	router.GET("/", index)

	router.Run(":" + port)
}
