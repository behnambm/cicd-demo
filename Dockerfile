FROM golang:1.17.11

RUN mkdir -p /src
WORKDIR /src

ADD templates ./templates
COPY go.mod ./
COPY go.sum ./
COPY *.go ./

RUN go mod download
RUN go build -o /src/app_exec

EXPOSE 80

CMD ["/src/app_exec"]