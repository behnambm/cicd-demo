#!/usr/bin/env sh


# shellcheck disable=SC2046
if [ $( go test -cover ./... | awk '{print $5}' | sed "s/%//" | cut -d. -f 1) -lt 65 ]; then
  exit 1
else
  exit 0
fi
