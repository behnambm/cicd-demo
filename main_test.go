package main

import (
	"bytes"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func SetUpRouter() *gin.Engine {
	router := gin.Default()
	return router
}

func SetUpRouterWithTemplates() *gin.Engine {
	router := gin.Default()
	router.LoadHTMLGlob("templates/*")
	return router
}

func TestPing(t *testing.T) {
	mockResponse := `{"status":"ok"}`
	r := SetUpRouter()
	r.GET("/ping", ping)
	req, _ := http.NewRequest("GET", "/ping", nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	responseData, _ := ioutil.ReadAll(w.Body)
	assert.Equal(t, mockResponse, string(responseData))
	assert.Equal(t, http.StatusOK, w.Code)
}

func TestIndex(t *testing.T) {
	r := SetUpRouterWithTemplates()
	r.GET("/", index)
	req, _ := http.NewRequest("GET", "/", nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	responseData, _ := ioutil.ReadAll(w.Body)
	assert.EqualValues(t, http.StatusOK, w.Code)
	assert.True(t, strings.Contains(string(responseData), `Login`))
}

func TestAuthBadFormBody(t *testing.T) {
	r := SetUpRouterWithTemplates()
	r.GET("/auth", auth)

	body := bytes.NewBufferString("user=admin&pass=admin")
	req, _ := http.NewRequest("GET", "/auth", body)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	responseData, _ := ioutil.ReadAll(w.Body)
	assert.EqualValues(t, http.StatusBadRequest, w.Code)
	assert.True(t, strings.Contains(string(responseData), `invalid request`))
}

func TestAuthInvalidCredentials(t *testing.T) {
	r := SetUpRouterWithTemplates()
	r.POST("/auth", auth)

	body := bytes.NewBufferString("username=user&password=myPass")
	req, _ := http.NewRequest("POST", "/auth", body)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	responseData, _ := ioutil.ReadAll(w.Body)
	assert.EqualValues(t, http.StatusUnauthorized, w.Code)
	assert.True(t, strings.Contains(string(responseData), `invalid username or password`))
}

func TestAuthValidCredentials(t *testing.T) {
	r := SetUpRouterWithTemplates()
	r.POST("/auth", auth)

	body := bytes.NewBufferString("username=admin&password=admin")
	req, _ := http.NewRequest("POST", "/auth", body)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	assert.EqualValues(t, http.StatusFound, w.Code)
	assert.EqualValues(t, "/admin", w.Header().Get("Location"))
	assert.EqualValues(t, "is_verified=true; Path=/admin; Max-Age=3600; HttpOnly", w.Header().Get("Set-Cookie"))
}

func TestNoCookieAdmin(t *testing.T) {
	r := SetUpRouterWithTemplates()
	r.GET("/admin", admin)

	req, _ := http.NewRequest("GET", "/admin", nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	responseData, _ := ioutil.ReadAll(w.Body)
	assert.EqualValues(t, http.StatusUnauthorized, w.Code)
	assert.True(t, strings.Contains(string(responseData), `not authorized`))
}

func TestInvalidCookieAdmin(t *testing.T) {
	r := SetUpRouterWithTemplates()
	r.GET("/admin", admin)

	req, _ := http.NewRequest("GET", "/admin", nil)
	req.AddCookie(&http.Cookie{
		Name:   "is_verified",
		MaxAge: 3600,
		Value:  "incorrect-value",
	})
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	responseData, _ := ioutil.ReadAll(w.Body)
	assert.EqualValues(t, http.StatusUnauthorized, w.Code)
	assert.True(t, strings.Contains(string(responseData), `not authorized`))
}

func TestValidAdmin(t *testing.T) {
	r := SetUpRouterWithTemplates()
	r.GET("/admin", admin)

	req, _ := http.NewRequest("GET", "/admin", nil)
	req.AddCookie(&http.Cookie{
		Name:     "is_verified",
		MaxAge:   3600,
		Value:    "true",
		HttpOnly: true,
		Secure:   true,
	})
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	responseData, _ := ioutil.ReadAll(w.Body)
	assert.EqualValues(t, http.StatusOK, w.Code)
	assert.True(t, strings.Contains(string(responseData), `hello admin`))
}
