
make build:
	echo "Build mode: ${GIN_MODE}"
	go build

test:
	go test ./...

coverage:
	chmod +x ./scripts/coverage.sh
	sh ./scripts/coverage.sh
