The live demo for this GitHub repository is no longer functional. However, the main concept is still visually represented in the image displayed below.

The primary goal of this repository was to provide a practical exercise in constructing a CI/CD pipeline from the ground up.


![Screenshot from 2023-04-25 14-13-25](https://user-images.githubusercontent.com/26994700/234254008-f6d85be7-c738-4dd9-8913-1530db0061da.png)

